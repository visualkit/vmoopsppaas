#!/bin/bash

##################################################################################
# Copyright Philippe Hayer contact@visualkit.com
# This file is part of vmoopsppaas.
#    This program is free software; you can redistribute it and/or modify it under the terms of the GNU Affero General Public License version 3 as published by the Free Software Foundation with the addition of the following permission added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK IN WHICH THE COPYRIGHT IS OWNED BY vmoopsppaas OWNER, vmoopsppaas OWNER DISCLAIMS THE WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero General Public License along with this program; if not, see http://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA, 02110-1301 USA, or download the license from the following URL: http://www.gnu.org/licenses/agpl.txt
#
#    The interactive user interfaces in modified source and object code versions of this program must display Appropriate Legal Notices, as required under Section 5 of the GNU Affero General Public License.
#
#    In accordance with Section 7(b) of the GNU Affero General Public License, you must retain the producer line in every service that is created or manipulated using vmoopsppaas or display a link to https://gitlab.com/visualkit/vmoopsppaas in any user interface.
#
#    You can be released from the requirements of the license by purchasing a commercial license. Buying such a license is mandatory as soon as you develop commercial activities, direct or not, involving the vmoopsppaas software without disclosing the source code of your own applications. These activities include with no limitation: offering paid services to customers as an ASP, serving Virtual Machines on the fly via  a web application, shipping vmoopsppaas with a closed source product.
###################################################################################


choix=${3:-'defaut'}
myPROMPT="\\$"

echo "# postinstall_creer_vm $choix"
# il FAUT utiliser id_rsa et non un fichier specifique, sinon un simple ssh echoue
case $choix in
copy_sshkey)
echo "# copier une clef ssh vm_$1 pour l'utilisateur $2 de $1 vers $4"
#ssh -t -oStrictHostKeyChecking=no -oCheckHostIP=no ${2}@${1} " ssh-copy-id -i ~/.ssh/vm_${1}.pub ${2}@${4}"
# ssh -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${2}@${1} " ssh-copy-id -o StrictHostKeyChecking=no ${2}@${4}"
#myPROMPT="\\$"
#set timeout 115
cat << EOD | /usr/bin/expect -d 
spawn ssh -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${2}@${1} 
expect "\\\\$"
send "ssh-copy-id -o StrictHostKeyChecking=no ${2}@${4} \n"
sleep 1
expect {
 "assword" {
 send  "$2\r" 
 }
 "$myPROMPT " {
 send "exit\r"
 }
 "already exist" {
 send_user "key already exist when copy ssh id for user ${2} to ${4}\n"
 }
 "try again" {
 send_user "password is not good when copy ssh id for user ${2} to ${4}\n"
 }

}
EOD
;;
gene_sshkey)
echo "# gene_sshkey : creer une clef ssh pour l'utilisateur $2 sur $1"
#ssh -t -oStrictHostKeyChecking=no -oCheckHostIP=no ${2}@${1} ' yes "no"|ssh-keygen  -N "" -f ~/.ssh/vm_'$1
##ssh -t -oStrictHostKeyChecking=no -oCheckHostIP=no ${2}@${1} " if [ `sudo grep 'unassigned' /home/${2}/.ssh/vm_${1}.pub |wc -l` -eq 1 ]; then sed 's/unassigned/${1}/' /home/${2}/.ssh/vm_${1}.pub >~/.ssh/vmmod_$1.pub;fi"
#ssh -t -oStrictHostKeyChecking=no -oCheckHostIP=no ${2}@${1} " sed "s/unassigned/${1}/" /home/${2}/.ssh/vm_${1}.pub >~/.ssh/vmmod_$1.pub"
##ssh -t -oStrictHostKeyChecking=no -oCheckHostIP=no ${2}@${1} " if [ -f ~/.ssh/vmmod_${1} ]; then mv ~/.ssh/vmmod_${1}.pub ~/.ssh/vm_${1}.pub ; fi"
#ssh -t -oStrictHostKeyChecking=no -oCheckHostIP=no ${2}@${1} " mv ~/.ssh/vmmod_${1}.pub ~/.ssh/vm_${1}.pub"
#ssh -t -oStrictHostKeyChecking=no -oCheckHostIP=no ${2}@${1} " chmod 0600 ~/.ssh/vm_${1}.pub"

#/usr/bin/expect - 
#set timeout 115
cat << EOD | /usr/bin/expect -d 
spawn ssh -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${2}@${1} 
expect "\\\\$"
send "ssh-keygen -b 2048 -t rsa -f ~/.ssh/id_rsa  -q\n"
sleep 1
expect {
 "passphrase):"
{
send "\r"
exp_continue
}
 "again:"
{
send "\r"
exp_continue
}
 "Overwrite" 
{
send "y\r"
}
 "$myPROMPT "
{
send "exit \r"}
}
EOD

cat << EOF > /tmp/fic_cmd_gene_sshkey.sh
#!/bin/bash
#yes "no"|ssh-keygen  -f ~/.ssh/vm_$1 -P ''
sed "s/unassigned/${1}/" /home/${2}/.ssh/id_rsa.pub >~/.ssh/vmmod_${1}.pub
mv ~/.ssh/vmmod_${1}.pub ~/.ssh/id_rsa.pub
#chmod 0600 ~/.ssh/id_rsa.pub
EOF
chmod 0777 /tmp/fic_cmd_gene_sshkey.sh
cat /tmp/fic_cmd_gene_sshkey.sh
ssh -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${2}@${1} bash < /tmp/fic_cmd_gene_sshkey.sh
;;


inst_sshpass)
echo "# installation de sshpass?"
ssh -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${2}@${1} bash < <(
cat << 'EOCMD' 
#!/bin/bash
test_sshpass=`sshpass -V|wc -l`
# echo "test sshpass : $test_sshpass<---"
if [ $test_sshpass -eq 3 ]
then
echo '  - sshpass deja disponible'
else
echo '  - installation de sshpass '
sudo apt-get install sshpass
fi
EOCMD
)
;;
add_sshkey)
echo "# ajout de la clef ssh $3 sur l'utilisateur $2"
sshpass -p $2 ssh-copy-id -i $4 -o StrictHostKeyChecking=no ${2}@${1}
;;

*)
### cas par defaut


test_utilisateur=`whoami`

if [ ${test_utilisateur} = ${2:-'vide'} ]
then
echo "# ajout de la clef ssh de l'utilisateur $2"
sleep 5
sshpass -p $2 ssh-copy-id -o StrictHostKeyChecking=no ${2}@${1}
echo "# ajout de l'utilisateur $2 pour sudo sans mot de passe"

ssh -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${2}@${1} "echo '$2 ALL=(ALL) NOPASSWD:ALL' > ${2}.suduser"

# ssh -t -oStrictHostKeyChecking=no -oCheckHostIP=no ${2}@${1} bash -c "echo '${2}'|sudo mv ${2}.suduser /etc/sudoers.d/$2"

echo "# renommer et changer les droits sur le fichier ${2}.suduser"
# /usr/bin/expect - <<EOD
# set timeout 115
cat << EOD | /usr/bin/expect -d 
spawn ssh -t -o StrictHostKeyChecking=no -o CheckHostIP=no ${2}@${1} 
sleep 1
send "sudo ls -l\r"
expect {
 "assword"
{
send  "$2\r" 
sleep 1
send_user "password saisi\n"
exp_continue
}
 "${2}.suduser"
{
send "sudo mv ${2}.suduser /etc/sudoers.d/$2\r"
expect {
 "assword"
{
send  "$2\r" 
sleep 1
send_user "password saisi\n"
exp_continue
}
 "$myPROMPT " {
send_user "ok ok\n"
}
}
send "sudo chown root /etc/sudoers.d/$2\r"
expect {
 "assword"
{
send  "$2\r" 
sleep 1
send_user "password saisi\n"
exp_continue
}
 "$myPROMPT " {
send_user "ok ok\n"
}
}
send "sudo chgrp root /etc/sudoers.d/$2\r"
expect {
 "assword"
{
send  "$2\r" 
sleep 1
send_user "password saisi\n"
exp_continue
}
 "$myPROMPT " {
send_user "ok ok\n"
}
}
send "sudo chmod 0440 /etc/sudoers.d/$2\r"
expect {
 "assword"
{
send  "$2\r" 
sleep 1
send_user "password saisi\n"
exp_continue
}
 "$myPROMPT " {
send_user "ok ok\n"
}
}
send "sudo ls -l /etc/sudoers.d\r"
send_user "sortie du bloc $ du sudo sans mot de passe\n"
}
}
send  "exit\n" 
EOD
echo " test final du sudo sans mot de passe"
#ssh -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${2}@${1} "sudo ls -l /etc/sudoers.d/${2}.suduser"
ssh -t -o StrictHostKeyChecking=no -o CheckHostIP=no ${2}@${1} "sudo ls -l /etc/sudoers.d/${2}"

else
echo "erreur : l'utilisateur $2 n'est pas l'utilisateur courant"
fi
echo "fin de l'initialisation de l'utilisateur $2 sur $1 "
;;
esac

