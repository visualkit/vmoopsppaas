#!/bin/bash
##################################################################################
# Copyright 2016 Philippe Hayer contact@visualkit.com
# This file is part of vmoopsppaas.
#    This program is free software; you can redistribute it and/or modify it under the terms of the GNU Affero General Public License version 3 as published by the Free Software Foundation with the addition of the following permission added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK IN WHICH THE COPYRIGHT IS OWNED BY vmoopsppaas OWNER, vmoopsppaas OWNER DISCLAIMS THE WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero General Public License along with this program; if not, see http://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA, 02110-1301 USA, or download the license from the following URL: http://www.gnu.org/licenses/agpl.txt
#
#    The interactive user interfaces in modified source and object code versions of this program must display Appropriate Legal Notices, as required under Section 5 of the GNU Affero General Public License.
#
#    In accordance with Section 7(b) of the GNU Affero General Public License, you must retain the producer line in every service that is created or manipulated using vmoopsppaas or display a link to https://gitlab.com/visualkit/vmoopsppaas in any user interface.
#
#    You can be released from the requirements of the license by purchasing a commercial license. Buying such a license is mandatory as soon as you develop commercial activities, direct or not, involving the vmoopsppaas software without disclosing the source code of your own applications. These activities include with no limitation: offering paid services to customers as an ASP, serving Virtual Machines on the fly via  a web application, shipping vmoopsppaas with a closed source product.
###################################################################################

nom_script="creer_labo_base"
rep_script='/srv/devops/vmoopsppaas/scripts'
#rep_script=`pwd`
rep_cible='base'
#rep_dest_loc_git="/srv/devops/labo"
rep_dest_loc_git="/srv/devops/labo"
#rep_dest_lab="/srv/devops/labo/$rep_cible"
rep_dest_lab="${rep_dest_loc_git}/$rep_cible"
#rep_dest_dist_git="/media/syno/prod/depot_git/infra/labo"
rep_dest_dist_git="/tmp/media/syno/prod/depot_git/infra/labo"
echo "nettoyer le depot git distant"
rm -rf $rep_dest_dist_git
nom_alias_git_distant='git_distant'
loctty=$(tty) #pour la sortie de tee sur l'entree std de ce script
#liste_serveurs='test_creer_vm1 srv_dev srv_git_lab srv_www'
vm_maitre_ansible='vm_ansible'
liste_serveurs="test_creer_vm1 $vm_maitre_ansible srv_git_lab"
default_user='phil'
default_user_key="/home/${default_user}/.ssh/id_rsa.pub"
vbox_version=`virtualbox --help|head -n1|cut -d' ' -f5`
vagrant_sshkey="/srv/phil/.ssh/192.168.1.15.pub"
indentvar=''
indentdicvar=''
maxindentvar=0
read -d '' json_serveurs << EOJSON
{
"test_creer_vm1":{
"disksize":2000
},
"$vm_maitre_ansible":{
"disksize":3000
},
"srv_git_lab":{
"disksize":2000
}
}
EOJSON
#echo $json_serveur|getJsonVal "[$nomserveur]['disksize']"
# --------------------------- Fonctions ------------------------------------
# http://stackoverflow.com/questions/369758/how-to-trim-whitespace-from-a-bash-variable
trim() {
    local var="$@"
    var="${var#"${var%%[![:space:]]*}"}"   # remove leading whitespace characters
    var="${var%"${var##*[![:space:]]}"}"   # remove trailing whitespace characters
    echo -n "$var"
}
# http://stackoverflow.com/questions/1955505/parsing-json-with-unix-tools
# echo '{"foo": {"bar": "baz", "a": [1,2,3]}}' |  getJsonVal "['foo']['a'][1]"
getJsonVal() {
   if [ \( $# -ne 1 \) -o \( -t 0 \) ]; then
       echo "Usage: getJsonVal 'key' < /tmp/file";
       echo "   -- or -- ";
       echo " cat /tmp/input | getJsonVal 'key'";
       return;
   fi;
   python -c "import json,sys;sys.stdout.write(json.dumps(json.load(sys.stdin)$1))";
}

. gene_yml.sh

init_ansible_cfg(){
cat << EOF >$rep_dest_lab/ansible.cfg
[defaults]
host_key_checking = False
record_host_keys=False
ssh_args_inactive = -o ControlMaster=auto -o ControlPersist=1862s

EOF
} # init_ansible_cfg()

init_ansible_hosts(){
cat << EOF >$rep_dest_lab/hosts
[labo_base_hosts]
EOF

echo "#construction du fichier d'inventaire"
local reps_host=${1:-'.'}
local nom_host=${2:-'hosts'}
local liste_actions="$3"
echo "# construction a partir de la liste d'action"
echo "$liste_actions"
gene_liste_serveurs "$reps_host" "$nom_host" "$liste_actions"

} # init_ansible_hosts()

test_vm_exists()
{
echo ".....tester si vm $1 existe"
local test_vm=`vboxmanage list vms|grep $1|cut -d'"' -f2`
#echo $test_vm
if [ ${test_vm:-'vide'} = 'vide' ]
then
echo 'result:nok'
else
if [ ${test_vm} = $1 ]
then
echo 'result:ok'
else
echo 'result:nok'
fi
fi
} # test_vm_exists()

test_vm_lancee()
{
echo ".....tester si vm $1 existe"
local test_vm=`vboxmanage list runningvms|grep $1|cut -d'"' -f2`
#echo $test_vm
if [ ${test_vm:-'vide'} = 'vide' ]
then
echo 'result:nok'
else
if [ ${test_vm} = $1 ]
then
echo 'result:ok'
else
echo 'result:nok'
fi
fi
} # test_vm_lancee()

lancer_vm(){
local nom_vm=$1
echo "# lancement de $nom_vm"
local val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
if [ $nom_vm != ${val:-"vide"} ] 
then
VBoxManage startvm "$nom_vm"
local val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
echo "  - lancement de la vm en cours"
while [ $nom_vm != ${val:-"vide"} ] 
do 
echo '#' |awk '{printf("%s", $0)}'
sleep 20;
local val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
done
echo "  - la vm est lancee"
else
echo "  - la vm est deja lancee"
fi
} # lancer_vm()

redemarrer_vm(){
local nom_vm=$1
echo "# redemarrer $nom_vm"
local val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
if [ $nom_vm = ${val:-"vide"} ] 
then
echo "  - arret de la vm en cours"
VBoxManage controlvm "$nom_vm" savestate
#VBoxManage controlvm "$nom_vm" poweroff
sleep 20;
local val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
while [ $nom_vm = ${val:-"vide"} ] 
do 
echo '#' |awk '{printf("%s", $0)}'
sleep 20;
local val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
done
else
echo "  - la vm est deja arretee"
fi
local val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
echo "  - lancement de la vm en cours"
VBoxManage startvm "$nom_vm"
while [ $nom_vm != ${val:-"vide"} ] 
do 
echo '#' |awk '{printf("%s", $0)}'
sleep 20;
local val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
done
echo "  - la vm est lancee"
} # redemarrer_vm()

get_adr_mac_ip(){
local nom_vm=$1
adr_mac=`vboxmanage showvminfo $nom_vm --details 2>&1 | grep 'NIC 1:' | sed -re 's/.*MAC: (.+), Attachment.*/\1/' -e 's/(\w{2})/\1:/g' -e 's/:$//'`
echo "# adresse mac de $nom_vm : $adr_mac"
adr_ip=`ip n show|tr [a-z] [A-Z]|grep $adr_mac|cut -d ' '  -f1`
echo "# adresse ip de $nom_vm : $adr_ip"
#retourner les informations formattees
echo "result:adr_mac_ip;$nom_vm;$adr_mac;$adr_ip"
} #get_adr_mac_ip(){

update_arp(){
echo "#mise a jour de la table arp"
adr_nmap=`ip r|grep default|cut -d' ' -f3|cut -d'.' -f1,2,3`.*
nmap -sP $adr_nmap
}

init_serveurs(){
echo "# initialisation de $liste_serveurs"
for nom_serveur in `echo $liste_serveurs` 
do
echo "# traitement de $nom_serveur"
# result_brut=`test_vm_exists $nom_serveur`
# echo "result_brut=$result_brut"
local result=`test_vm_exists $nom_serveur|grep result:|cut -d':' -f2`
echo "result=$result"
case $result in
nok)
echo "# creer la vm $nom_serveur"
#result_init_serveurs=$($rep_script/creer_vm.sh $nom_serveur 2>&1) |tee ${loctty}
tailledisk=`echo $json_serveurs|getJsonVal "['$nom_serveur']['disksize']"`
echo "# ---> taille du disque: $tailledisk"
#echo $json_serveurs
$rep_script/creer_vm.sh $nom_serveur ${tailledisk:-2000}
;;
ok)
echo "# lancer la vm $nom_serveur"
#result_init_serveurs=$(lancer_vm $nom_serveur 2>&1) |tee ${loctty}
lancer_vm $nom_serveur
;;
*)
echo "# cas a probleme $result"
exit 1
esac
done # for nom_serveur in $liste_serveur
echo $result_init_serveurs
} # init_serveurs()
#echo "result:adr_mac:$nom_vm:$adr_mac"
#echo "result:adr_ip:$nom_vm:$adr_ip"

construire_ansible_hosts(){
echo "# construction du hosts de ansible pour $liste_serveurs"
update_arp
for nom_serveur in `echo $liste_serveurs` 
do
echo "# ajout dans hosts ansible :"
#local result_get=$(get_adr_mac_ip $nom_serveur 2>&1) |tee ${loctty}
local result_get=$(get_adr_mac_ip $nom_serveur |grep 'result:')
echo $result_get
local nom_vm_get=$(echo $result_get|cut -d';' -f2)
local adr_mac=$(echo $result_get|cut -d';' -f3)
local adr_ip=$(echo $result_get|cut -d';' -f4)
echo $adr_ip >> $rep_dest_lab/hosts
done # for nom_serveur in $liste_serveur
} # construire_ansible_hosts()

executer_post_install_vms(){
echo "# postinstallation des vms pour $liste_serveurs"

rm $rep_dest_lab/liste_serveurs.txt
update_arp
for nom_serveur in `echo $liste_serveurs` 
do
echo "# postinstall de $nom_serveur :"
local result_get=$(get_adr_mac_ip $nom_serveur |grep 'result:')
echo $result_get
local nom_vm_get=$(echo $result_get|cut -d';' -f2)
local adr_mac=$(echo $result_get|cut -d';' -f3)
local adr_ip=$(echo $result_get|cut -d';' -f4)
while [ ${adr_ip:-'vide'} = 'vide' ]
do
echo " --- la vm $nom_serveur ne semble pas disponible ... patientez"
date
sleep 30
update_arp
result_get=$(get_adr_mac_ip $nom_serveur |grep 'result:')
nom_vm_get=$(echo $result_get|cut -d';' -f2)
adr_mac=$(echo $result_get|cut -d';' -f3)
adr_ip=$(echo $result_get|cut -d';' -f4)
done
echo " --- la vm $nom_serveur a l'ip $adr_ip"
local cmd="ls -l /etc/sudoers.d/$1"
local user_fic=`ssh -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${1}@${adr_ip} "$cmd"|cut -d' ' -f3 `
local test_warning_tmp=`ssh -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${1}@${adr_ip} "$cmd" 2>&1|grep 'WARNING' `
local test_warning=`echo $test_warning_tmp|tr -s ' '|cut -d' ' -f2|cut -d':' -f1`
cd $rep_script
if [ ${user_fic:-'vide'} != 'root' ]
then
echo "  - suppression de la cle ssh de $adr_ip"
ssh-keygen -f "/srv/phil/.ssh/known_hosts" -R $adr_ip
./postinstall_creer_vm.sh $adr_ip $1
redemarrer_vm $nom_vm_get
else
if [ ${test_warning:-'vide'} = 'WARNING' ]
then
echo "  - supprimer la vieille cle ssh de  $adr_ip"
ssh-keygen -f "/srv/phil/.ssh/known_hosts" -R $adr_ip
./postinstall_creer_vm.sh $adr_ip $1
redemarrer_vm $nom_vm_get
else
echo "  - l'utilisateur est deja pret"
fi
fi
# nettoyer known_host
ssh-keygen -R $adr_ip

./postinstall_creer_vm.sh $adr_ip $1 'inst_sshpass'
echo "$result_get">>$rep_dest_lab/liste_serveurs.txt
done # for nom_serveur in $liste_serveur
} # executer_post_install_vms()

installer_ansible_vm(){
echo "# installation de ansible pour $vm_maitre_ansible"
local result_get=$(get_adr_mac_ip $vm_maitre_ansible |grep 'result:')
echo $result_get
local nom_vm_get=$(echo $result_get|cut -d';' -f2)
local adr_mac=$(echo $result_get|cut -d';' -f3)
local adr_ip=$(echo $result_get|cut -d';' -f4)
ssh -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${1}@${adr_ip} bash <./init_ansible.sh
} # installer_ansible_vm()

executer_cmd_ansible_vm(){
echo "# executer le script $2 sur la vm ansible $vm_maitre_ansible"
local result_get=$(get_adr_mac_ip $vm_maitre_ansible |grep 'result:')
echo $result_get
local nom_vm_get=$(echo $result_get|cut -d';' -f2)
local adr_mac=$(echo $result_get|cut -d';' -f3)
local adr_ip=$(echo $result_get|cut -d';' -f4)
# supprimer les anciennes adresses de known_host pour eviter le warning 
# remote host identification has changed
ssh-keygen -f "/srv/phil/.ssh/known_hosts" -R $adr_ip
#ssh -t -oStrictHostKeyChecking=no -oCheckHostIP=no ${1}@${adr_ip} bash <( ${rep_script}/${2} $3 )
ssh -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${1}@${adr_ip} bash < ${rep_script}/${2} 
} # installer_ansible_vm()

upd_depot_distant_git(){
local depot_distant="${rep_cible}.git"
echo "# mise a jour du depot distant $depot_distant"
local loc_rep_dest_dist_git="${rep_dest_dist_git}/${depot_distant}"
local depot_local=${rep_cible}
local loc_rep_dest_loc_git="${rep_dest_loc_git}/${depot_local}"
local nom_alias=$nom_alias_git_distant
if [ ! -d $loc_rep_dest_dist_git ]
then
echo "# le depot distant n'existe pas : le creer"
mkdir -p $loc_rep_dest_dist_git
cd $rep_dest_dist_git
git init $depot_distant --bare 
fi

echo "# alimenter le depot distant $depot_distant"
cd $loc_rep_dest_loc_git
git remote -v
git push $nom_alias master
cd $rep_script
} # upd_depot_distant_git()

upd_depot_local_git(){
local depot_local=${rep_cible}
echo "# mise a jour du depot local $depot_local"
local loc_rep_dest_loc_git="${rep_dest_loc_git}/${depot_local}"
local loc_rep_dest_loc_git_test="${rep_dest_loc_git}/${depot_local}/.git"
local depot_distant="${rep_cible}.git"
local loc_rep_dest_dist_git="${rep_dest_dist_git}/${depot_distant}"
local srv_dist="ssh://$default_user@localhost$loc_rep_dest_dist_git"
local nom_alias=$nom_alias_git_distant
if [ ! -d $loc_rep_dest_loc_git_test ]
then
echo " - le depot local n'existe pas : creer $loc_rep_dest_loc_git"
cd $loc_rep_dest_loc_git
git init 
fi
echo " # mise a jour de l'alias $nom_alias sur $srv_dist"
cd $loc_rep_dest_loc_git
pwd
git remote -v
if [ `git remote|grep "$nom_alias"|wc -l` -eq 1 ]
then
echo " -suppression alias $nom_alias sur serveur distant"
git remote remove $nom_alias
else
echo " pas de $nom_alias ???" 
fi
if [ `git remote|grep "$nom_alias"|wc -l` -lt 1 ]
then
echo " -ajout alias $nom_alias sur serveur distant $srv_dist"
git remote add $nom_alias $srv_dist
fi
echo " - commiter le depot local $depot_local"
git add *
git commit -m "mise a jour auto $nom_script/$(date +%Y%m%d.%H%M)"
cd $rep_script
} # upd_depot_local_git()
# --------------------------- commandes ------------------------------------
date_debut=`date`
echo "# debut $nom_script a $date_debut"
init_ansible_cfg
#init_ansible_hosts

read -d '' liste_actions <<LISTE
HOST;hosts;./
INITLSTVM;
VM;test_creer_vm1;DYNIP;target_host=test_vm1
VM;srv_git_lab;DYNIP
VM;vm_ansible;DYNIP;target_host=ctrlr_ansible
GRP;docker
 GVM;vm_ansible
GRP;maitre_ansible
 GVM;vm_ansible
GRP;autres
 GVM;test_creer_vm1
 GVM;srv_git_lab
GDG;labo_base_hosts
 GEL;docker
 GEL;maitre_ansible
 GEL;autres
GVARS;labo_base_hosts
 GVAR;mon_groupe;datagroupe

LISTE

gene_liste_serveurs "$liste_actions"

init_serveurs
executer_post_install_vms $default_user
# construire_ansible_hosts
gene_hosts "$liste_actions"
installer_ansible_vm $default_user
cat $rep_dest_lab/hosts

echo "# Construction du role update"
reps_yml='roles/update/tasks'
nom_yml='main.yml'
mkdir -p ${rep_dest_lab}/${rep_yml}

read -d '' liste_actions <<LISTE
INIT
 APT_REP;activer depot backport;deb http://http.debian.net/debian jessie-backports main;state=present
 APT;apt-get update du serveur;update_cache=yes cache_valid_time=3600
 APT;installation de docker;name=docker.io state=present
 APT;installation des outils pythons;name=python-setuptools state=present
 INST_PY;installer pip;pip;state=present
 PIP;virtualenv docker-py
CHREP;RPL;roles/vagrant/tasks
INIT;
 CREERSUDO;adm_no_pwd;vagrant
 LAUTH_KEY;vagrant;$default_user_key
 APT;installation de debconf-utils;name=debconf-utils state=present
 APT;installation de dkms;name=dkms state=present
 ACT;stat;path=/tmp/VBoxGuestAdditions_${vbox_version}.iso
  REGIST;datafic
 SHELL;test presence de VBoxLinuxAdditions;lsmod|grep -i vboxfs|wc -l 
  REGIST;vboxfs
 ACT;get_url;url=http://download.virtualbox.org/virtualbox/${vbox_version}/VBoxGuestAdditions_${vbox_version}.iso dest=/tmp mode=0440
  WHEN;(not datafic.stat.exists) and vboxfs.stdout < 1
 ACT;mount;name=/mnt opts=loop,ro fstype=iso9660 state=mounted src=/tmp/VBoxGuestAdditions_${vbox_version}.iso
  WHEN;datafic.stat.exists
 SHELL;Ajout des additions VBOX linux;echo \$(/mnt/VBoxLinuxAdditions.run);true
  WHEN;(datafic.stat.exists and vboxfs.stdout < 1)
 ACT;command;umount /mnt
  WHEN;datafic.stat.exists
 ACT;file;path=/tmp/VBoxGuestAdditions_${vbox_version}.iso state=absent
 ACT;lineinfile;dest=/etc/fstab regexp=".*VBoxGuestAdditions.*" state=absent
CHREP;RPL;roles/mqtt/tasks
INIT;
 APT;installation de mosquitto mqtt;name=mosquitto state=present
 PIP;paho-mqtt
LISTE
echo "# construction a partir de la liste d'action"
echo "$liste_actions"
gene_yml "$reps_yml" "$nom_yml" "$liste_actions"
cat $rep_dest_lab/$reps_yml/$nom_yml

echo "#construction du playbook premier"
reps_yml='.'
nom_yml='premier.yml'

read -d '' liste_actions <<LISTE
INIT
HOSTS;labo_base_hosts
 RUSER;$default_user
 SUDO;yes
 ROLES;update
HOSTS;maitre_ansible
 RUSER;$default_user
 SUDO;yes
 ROLES;vagrant mqtt
HOSTS;test_creer_vm1
 RUSER;$default_user
 SUDO;yes
 ROLES;jenkins_host

LISTE
echo "# construction du playbook a partir de la liste d'action"
echo "$liste_actions"
gene_yml "$reps_yml" "$nom_yml" "$liste_actions"
cat $rep_dest_lab/$reps_yml/$nom_yml

echo "#construction des requis"
reps_yml='.'
nom_yml='requirements.yml'

read -d '' liste_actions <<LISTE
INIT
REQUI;geerlingguy.jenkins
 RQROL;jenkins_host
LISTE
echo "# construction des requis a partir de la liste d'action"
echo "$liste_actions"
gene_yml "$reps_yml" "$nom_yml" "$liste_actions"
cat $rep_dest_lab/$reps_yml/$nom_yml



cd $rep_dest_lab

# creer l'acces au serveur local host via ssh
ssh-copy-id -o StrictHostKeyChecking=no ${default_user}@localhost

upd_depot_local_git
upd_depot_distant_git

echo " ********** PREPARER LE SERVEUR ANSIBLE                **************"
ip_ansible_tmp=`get_adr_mac_ip $vm_maitre_ansible`
#echo "*********** debug : $ip_ansible_tmp <-----------"
ip_ansible=`get_adr_mac_ip $vm_maitre_ansible|grep result|cut -d';' -f4`
ip_locale=`ip r|grep -v virbr|tr -s ''|cut -d' ' -f12|tail -n1`
test_sshkey_ip_ansible=`cat ~/.ssh/authorized_keys|grep "$ip_ansible"|wc -l`
if [ $test_sshkey_ip_ansible -ne 1 ]
then
./postinstall_creer_vm.sh $ip_ansible $default_user 'gene_sshkey'
echo " #### la clef ssh de ip ansible $ip_ansible n'existe pas "
./postinstall_creer_vm.sh $ip_ansible $default_user 'copy_sshkey' $ip_locale
else
echo "# ***** la cle du serveur ansible existe deja en local. Verifiez si elle est coherente *****"
fi

echo " ********** LANCEMENT DU SCRIPT SUR LE SERVEUR ANSIBLE **************"
executer_cmd_ansible_vm $default_user 'exec_ansible_vm01.sh' $rep_cible

./postinstall_creer_vm.sh $ip_ansible vagrant 'add_sshkey' $vagrant_sshkey

echo "# informations sur les VM"
cat $rep_dest_lab/liste_serveurs.txt


echo " debut du script a $date_debut"
date_fin=`date`
echo " fin du script a $date_fin"
echo "################### fin $nom_script ##################"

