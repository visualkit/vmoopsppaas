#!/bin/bash

##################################################################################
# Copyright 2016 Philippe Hayer contact@visualkit.com
# This file is part of vmoopsppaas.
#    This program is free software; you can redistribute it and/or modify it under the terms of the GNU Affero General Public License version 3 as published by the Free Software Foundation with the addition of the following permission added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK IN WHICH THE COPYRIGHT IS OWNED BY vmoopsppaas OWNER, vmoopsppaas OWNER DISCLAIMS THE WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero General Public License along with this program; if not, see http://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA, 02110-1301 USA, or download the license from the following URL: http://www.gnu.org/licenses/agpl.txt
#
#    The interactive user interfaces in modified source and object code versions of this program must display Appropriate Legal Notices, as required under Section 5 of the GNU Affero General Public License.
#
#    In accordance with Section 7(b) of the GNU Affero General Public License, you must retain the producer line in every service that is created or manipulated using vmoopsppaas or display a link to https://gitlab.com/visualkit/vmoopsppaas in any user interface.
#
#    You can be released from the requirements of the license by purchasing a commercial license. Buying such a license is mandatory as soon as you develop commercial activities, direct or not, involving the vmoopsppaas software without disclosing the source code of your own applications. These activities include with no limitation: offering paid services to customers as an ASP, serving Virtual Machines on the fly via  a web application, shipping vmoopsppaas with a closed source product.
###################################################################################
set -x
# exec_ansible_vm01.sh
rep_cible='base'
depot_local=${rep_cible}
#rep_dest="test_git"
nom_alias="srv_dist_git"
default_user='phil'
default_user_key="/home/${default_user}/.ssh/id_rsa.pub"

rep_dest_dist_git="/tmp/media/syno/prod/depot_git/infra/labo"
#rep_dest_dist_git="/media/syno/prod/depot_git/infra/labo"
depot_distant=$rep_cible
ip_depot_distant='192.168.1.103'
echo "#### execution de exec_ansible_vm01.sh avec depot distant $ip_depot_distant"
loc_rep_dest_dist_git="${rep_dest_dist_git}/${depot_distant}"

srv_dist="ssh://$default_user@${ip_depot_distant}$loc_rep_dest_dist_git"

# rep_dest_loc_git="/tmp/devops/labo" # effac3 au reboot
rep_dest_loc_git="/devops/labo"
sudo mkdir -p /devops/
sudo mkdir -p $rep_dest_loc_git
sudo ls -al $rep_dest_loc_git
sudo chmod 0777 /devops/

upd_depot_local_git(){
local depot_local=${rep_cible}
echo "# mise a jour du depot local $depot_local"
local loc_rep_dest_loc_git="${rep_dest_loc_git}/${depot_local}"
local loc_rep_dest_loc_git_test="${rep_dest_loc_git}/${depot_local}/.git"
local depot_distant="${rep_cible}.git"
local loc_rep_dest_dist_git="${rep_dest_dist_git}/${depot_distant}"
local srv_dist="ssh://$default_user@${ip_depot_distant}$loc_rep_dest_dist_git"
# local srv_dist="$default_user@${ip_depot_distant}$loc_rep_dest_dist_git"
echo "# nettoyer known_host"
ssh-keygen -R $ip_depot_distant
ssh-keygen -R ${ip_depot_distant}$loc_rep_dest_dist_git
if [ ! -d $loc_rep_dest_loc_git_test ]
then
echo " - le depot local n'existe pas : le creer"
 mkdir -p $loc_rep_dest_loc_git
 cd $loc_rep_dest_loc_git
 git init 
fi
cd ..
cd $loc_rep_dest_loc_git
git remote remove $nom_alias 

if [ `git remote|grep "$nom_alias"|wc -l` -lt 1 ]
then
echo " -ajout alias $nom_alias sur serveur distant $srv_dist"
git remote add $nom_alias $srv_dist
git remote -v
fi 
echo " - cloner le depot distant $srv_dist"
pwd
#yes ''|git fetch $nom_alias 
#cd ..
echo "suppression de $rep_dest_loc_git pour test"
rm -r $rep_dest_loc_git
mkdir -p $rep_dest_loc_git
cd $rep_dest_loc_git
echo "initilisation de le conexion ssh avec la clef distante de git"
gituserpassword='phil1234'
gitusername='phil'
gitserverip=$ip_depot_distant
sshpass -p $gituserpassword ssh-copy-id -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no ${gitusername}@${gitserverip}
#sshpass -p $gituserpassword ssh-copy-id -i $default_user_key -o StrictHostKeyChecking=no ${gitusername}@${gitserverip}
# from https://superuser.com/questions/232373/how-to-tell-git-which-private-key-to-use/232375
echo 'ssh -i ~/.ssh/id_rsa -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $*' > ssh_git.sh
#echo "ssh -i $default_user_key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \$*" > ssh_git.sh
chmod +x ssh_git.sh
#GIT_SSH=`./ssh_git.sh`
GIT_SSH='./ssh_git.sh'
# git clone $srv_dist /tmp/test5
echo "# clonage du depot avec"
cat ./ssh_git.sh
$(GIT_TRACE=1 GIT_SSH='./ssh_git.sh' git clone $srv_dist) 
#$(GIT_SSH='./ssh_git.sh' git clone $srv_dist) 
# cd $rep_script
echo "--fin upd_depot_local_git"
} # upd_depot_local_git()




uname -a
utilisateur=`whoami`
rep_dest=$rep_dest_loc_git
echo "### execution distante"
echo " - suppression de $rep_dest "
if [ -d $rep_dest ]
then
rm -r $rep_dest
fi

echo " - creation de $rep_dest"
mkdir -p $rep_dest
cd $rep_dest
upd_depot_local_git

cd ${rep_dest}/$depot_local
if [ `git remote|grep "$nom_alias"|wc -l` -lt 1 ]
then
echo " -ajout alias sur serveur distant"
git remote add $nom_alias $srv_dist
else
 extract_nomalias=`git remote|grep "$nom_alias"`
echo "alias trouvé : $extract_nomalias"
fi

cat liste_serveurs.txt |while read ligne_serveur
do
result_get=$(echo $ligne_serveur |grep 'result:')
nom_vm_get=$(echo $result_get|cut -d';' -f2)
adr_mac=$(echo $result_get|cut -d';' -f3)
adr_ip=$(echo $result_get|cut -d';' -f4)

echo "# copier une clef ssh  pour l'utilisateur $utilisateur  vers $adr_ip"
sshpass -p $utilisateur ssh-copy-id -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no ${utilisateur}@${adr_ip}
done 
echo "# copier une clef ssh  pour l'utilisateur $utilisateur  vers localhost"
sshpass -p $utilisateur ssh-copy-id  -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no ${utilisateur}@localhost

echo "#### debut ansible####"
echo "### Installarion des pre-requis "
if [ -f requirements.yml ]
then
ansible-galaxy install -r requirements.yml -p ./roles/
fi
#ansible-playbook -i hosts premier.yml -kK --sudo-user $default_user | tee /dev/tty1
# ansible-playbook -vvvv -i hosts premier.yml --remote-user $default_user | tee /dev/tty1
echo "### lancement du playbook "
ansible-playbook -kK -i hosts premier.yml | tee /dev/tty1

# fin de exec_ansible_vm01.sh
