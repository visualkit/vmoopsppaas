#!/bin/bash

##################################################################################
# Copyright 2016 Philippe Hayer contact@visualkit.com
# This file is part of vmoopsppaas.
#    This program is free software; you can redistribute it and/or modify it under the terms of the GNU Affero General Public License version 3 as published by the Free Software Foundation with the addition of the following permission added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK IN WHICH THE COPYRIGHT IS OWNED BY vmoopsppaas OWNER, vmoopsppaas OWNER DISCLAIMS THE WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero General Public License along with this program; if not, see http://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA, 02110-1301 USA, or download the license from the following URL: http://www.gnu.org/licenses/agpl.txt
#
#    The interactive user interfaces in modified source and object code versions of this program must display Appropriate Legal Notices, as required under Section 5 of the GNU Affero General Public License.
#
#    In accordance with Section 7(b) of the GNU Affero General Public License, you must retain the producer line in every service that is created or manipulated using vmoopsppaas or display a link to https://gitlab.com/visualkit/vmoopsppaas in any user interface.
#
#    You can be released from the requirements of the license by purchasing a commercial license. Buying such a license is mandatory as soon as you develop commercial activities, direct or not, involving the vmoopsppaas software without disclosing the source code of your own applications. These activities include with no limitation: offering paid services to customers as an ASP, serving Virtual Machines on the fly via  a web application, shipping vmoopsppaas with a closed source product.
###################################################################################

nom_vm=${1:-"test_creation_debian"}
taille_hd=${2:-"2000"}
image_iso=${3:-"/srv/devops/isos/debian-8.3-amd64-CD-1.iso"}
# dest_vm="/srv/devops/vms/"
dest_vm='/srv/phil/VirtualBox VMs/'
# suppression de la vm si elle existe deja
if [ -f "${dest_vm}${nom_vm}.vdi" ]
then
vboxmanage unregistervm "$nom_vm" --delete
if [ -f "${dest_vm}${nom_vm}.vdi" ]
then
rm "${dest_vm}${nom_vm}.vdi"
fi
fi
echo '# creation de la vm'
VBoxManage createvm --name "$nom_vm" --ostype Debian_64 --register
#VBoxManage modifyvm "$nom_vm" --memory 512 --boot1 dvd --nic1 nat
VBoxManage modifyvm "$nom_vm" --memory 512 --boot1 dvd 
VBoxManage modifyvm "$nom_vm" --nic1 bridged --nictype1 82540EM --bridgeadapter1 eth0
echo "# ajout d'un disque"
VBoxManage createhd --filename "$dest_vm$nom_vm" --size $taille_hd
VBoxManage storagectl "$nom_vm" --name "SATA Controller" --add sata --controller IntelAHCI
VBoxManage storageattach "$nom_vm" --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium "${dest_vm}${nom_vm}.vdi"
echo "# ajout du cdrom sur l'image d'initialisation"
VBoxManage storagectl "$nom_vm" --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach "$nom_vm" --storagectl "IDE Controller" --port 0 --device 1 --type dvddrive --medium "$image_iso" 
echo "# demarrer la vm pour initialisation sur le cdrom"
VBoxManage startvm "$nom_vm"
echo "# attendre la fin de l'initialisation de la vm"
val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
echo "val=$val"
while [ $nom_vm = ${val:-"vide"} ] 
do 
sleep 20;
echo '.' |awk '{printf("%s", $0)}'
val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
done 
echo "."
sleep 20;
echo "# raccorder la connexion de la vm au reseau interne"
#VBoxManage modifyvm "$nom_vm" --nic1 bridged --nictype1 82540EM --bridgeadapter1 eth0
echo '# redemarrage de la vm'
VBoxManage startvm "$nom_vm"
val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
while [ $nom_vm != ${val:-"vide"} ] 
do 
echo '#' |awk '{printf("%s", $0)}'
sleep 20;
val=`vboxmanage list runningvms |grep $nom_vm |cut -d '"' -f2`
done
echo '#' 
echo "# attendre le demarrage de l'os" 
sleep 50;
echo "#mise a jour de la table arp"
adr_nmap=`ip r|grep default|cut -d' ' -f3|cut -d'.' -f1,2,3`.*
nmap -sP $adr_nmap
# adr_ip=`arp -a |tr -d \(|tr -d \)|tr [a-z] [A-Z]|grep \`vboxmanage showvminfo $nom_vm --details 2>&1 | grep 'NIC 1:' | sed -re 's/.*MAC: (.+), Attachment.*/\1/' -e 's/(\w{2})/\1:/g' -e 's/:$//'\`|cut -d ' '  -f2`
adr_mac=`vboxmanage showvminfo $nom_vm --details 2>&1 | grep 'NIC 1:' | sed -re 's/.*MAC: (.+), Attachment.*/\1/' -e 's/(\w{2})/\1:/g' -e 's/:$//'`
echo "# adresse mac de $nom_vm : $adr_mac"
adr_ip=`ip n show|tr [a-z] [A-Z]|grep $adr_mac|cut -d ' '  -f1`
echo "# adresse ip de $nom_vm : $adr_ip"
#retourner les informations formattees
echo "result:adr_mac:$nom_vm:$adr_mac"
echo "result:adr_ip:$nom_vm:$adr_ip"
