#!/bin/bash
##################################################################################
# Copyright 2016 Philippe Hayer contact@visualkit.com
# This file is part of vmoopsppaas.
#    This program is free software; you can redistribute it and/or modify it under the terms of the GNU Affero General Public License version 3 as published by the Free Software Foundation with the addition of the following permission added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK IN WHICH THE COPYRIGHT IS OWNED BY vmoopsppaas OWNER, vmoopsppaas OWNER DISCLAIMS THE WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero General Public License along with this program; if not, see http://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA, 02110-1301 USA, or download the license from the following URL: http://www.gnu.org/licenses/agpl.txt
#
#    The interactive user interfaces in modified source and object code versions of this program must display Appropriate Legal Notices, as required under Section 5 of the GNU Affero General Public License.
#
#    In accordance with Section 7(b) of the GNU Affero General Public License, you must retain the producer line in every service that is created or manipulated using vmoopsppaas or display a link to https://gitlab.com/visualkit/vmoopsppaas in any user interface.
#
#    You can be released from the requirements of the license by purchasing a commercial license. Buying such a license is mandatory as soon as you develop commercial activities, direct or not, involving the vmoopsppaas software without disclosing the source code of your own applications. These activities include with no limitation: offering paid services to customers as an ASP, serving Virtual Machines on the fly via  a web application, shipping vmoopsppaas with a closed source product.
###################################################################################

gene_liste_serveurs(){
local liste_hosts="$1"
local loc_liste_serveurs=''
local init=0
while read -r ligne
do
# echo "ligne=$ligne"
champ1=`trim $(echo $ligne|cut -d';' -f1)`
champ2=$(echo $ligne|cut -d';' -f2)
champ3=$(echo $ligne|cut -d';' -f3)
champ4=$(echo $ligne|cut -d';' -f4)
# echo "champ1=$champ1"
case $champ1 in
"VM")
loc_liste_serveurs=" $loc_liste_serveurs $champ2 "
;;
"INITLSTVM")
init=1
;;
esac
done < <(echo "$liste_hosts")  # while read ligne
if [ $init -eq 1 ]
then
liste_serveurs=`echo "$loc_liste_serveurs" |tr -s ' '`
else
if [ ${liste_serveurs:-'vide'} = 'vide'  ]
then
liste_serveurs=`echo "$loc_liste_serveurs" |tr -s ' '`
else
liste_serveurs=`echo " $liste_serveurs $loc_liste_serveurs" |tr -s ' '`
fi
fi
echo "# liste des serveurs :"
echo $liste_serveurs
} # gene_liste_serveurs()

gene_hosts(){
local reps_hosts='./'
local nom_fic_hosts='hosts'
local liste_hosts="$1"
local init=1
while read -r ligne
do
# echo "ligne=$ligne"
champ1=`trim $(echo $ligne|cut -d';' -f1)`
champ2=$(echo $ligne|cut -d';' -f2)
champ3=$(echo $ligne|cut -d';' -f3)
champ4=$(echo $ligne|cut -d';' -f4)
# echo "champ1=$champ1"
case $champ1 in
"VM")
if [ ${champ3:-'vide'} = 'DYNIP' ]
then
local result_get=$(get_adr_mac_ip $champ2 |grep 'result:')
echo $result_get
local nom_vm_get=$(echo $result_get|cut -d';' -f2)
local adr_mac=$(echo $result_get|cut -d';' -f3)
local adr_ip=$(echo $result_get|cut -d';' -f4)
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts
$champ2 ansible_host=$adr_ip $champ4
EOF
else
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts
$champ2 ansible_host=$champ2 $champ4
EOF
fi
;;
"INIT"|"HOST")
if [ ${champ3:-'vide'} != 'vide' ]
then
if [ ${champ2:-'vide'} != 'vide' ]
then
nom_fic_hosts=${champ3}$(trim $champ2)
else
echo "  --- le fichier d'inventaire reste $nom_fic_hosts"
fi
else
nom_fic_hosts=$(trim $champ2)
fi
cat << EOF > $rep_dest_lab/$reps_hosts/$nom_fic_hosts
---

EOF
;;
"GRP")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts

[$champ2]
EOF
;;
"GDG")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts

[$champ2:children]
EOF
;;
"GVARS")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts

[$champ2:vars]
EOF
;;
"GVAR")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts
$champ2=$champ3
EOF
;;
"GVM"|"GEL")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts
$champ2
EOF
;;
esac
done < <(echo "$liste_hosts")  # while read ligne
} # gene_hosts()

gene_yml(){
local reps_yml=$1
local nom_yml=$2
local liste_yml="$3"
local indent=''
local maxindent=''
# echo "gene_yml : $liste_yml"
while read -r ligne
do
# echo "ligne=$ligne"
champ1=`trim $(echo $ligne|cut -d';' -f1)`
champ2=$(echo $ligne|cut -d';' -f2)
champ3=$(echo $ligne|cut -d';' -f3)
champ4=$(echo $ligne|cut -d';' -f4)
champ5=$(echo $ligne|cut -d';' -f5)
champ6=$(echo $ligne|cut -d';' -f6)
# echo "champ1=$champ1"
case $champ1 in
"INDENT")
maxindent=$champ2
indent=$(for ((i=0;i<$maxindent;i++));do printf "   "; done;);
;;
"INIT")
cat << EOF > $rep_dest_lab/$reps_yml/$nom_yml
---

EOF
;;
"APT")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}- name: $champ2
${indent}  apt: $champ3

EOF
;;
"APT_REP")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}- name: $champ2
${indent}  apt_repository: repo='$champ3' 
${indent}    $champ4

EOF
;;
"INST_PY")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}- name: $champ2
${indent}  easy_install: name=" $champ3 " 
    $champ4

EOF
;;
"PIP")
# http://stackoverflow.com/questions/31396130/ansible-install-multiple-python-packages-on-a-single-session
# PIP;virtualenv==1.11.6 pss requests comment-builder
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}- name: "installation des paquets python : $champ2"
${indent}  sudo: true
${indent}  pip: name=" $champ2 "

EOF
;;
"SHELL")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml

${indent}- name: $champ2
${indent}  action: shell $champ3
EOF
if [ "${champ3-'vide'}" != 'vide'  ]
then
local val_shell_case=`echo $champ3|tr [a-z] [A-Z]`
local val_shell_ignore_err='False'
case $val_shell_case in
TRUE|VRAI|OUI|YES)
val_shell_ignore_err='True'
;;
esac
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}  ignore_errors: $val_shell_ignore_err
EOF
fi
;;
"REQUI")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}- src: $champ2
EOF
;;
"RQROL")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}  name: $champ2
EOF
;;
"RQVER")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}  version: $champ2
EOF
;;
"RQSCM")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}  scm: $champ2
EOF
;;
"SUDO")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}  sudo: $champ2
EOF
;;
"WHEN")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}  when: $champ2
EOF
;;
"REGIST")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}  register: $champ2
EOF
;;
"ACT")
nom_act=`trim $champ2`
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}- name: Executer $champ2 $champ3
${indent}  $nom_act: $champ3
EOF
;;
"MSG")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}-debug: msg="$champ2"
EOF
;;
"HOSTS")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}- hosts: $champ2
EOF
;;
"CHFIC")
nom_yml=$champ2
;;
"CHREP")
if [ ${champ2-'vide'} != 'vide'  ]
then
local val_chrep_case=`echo $champ2|tr [a-z] [A-Z]`
case $val_chrep_case in
"RPL")
reps_yml=$champ3
mkdir -p $rep_dest_lab/$reps_yml
;;
"ADD")
reps_yml="$reps_yml/$champ3"
mkdir -p $rep_dest_lab/$reps_yml
;;
esac
echo "# Creation du repertoire $rep_dest_lab/$reps_yml"
echo "# et ecriture dans $rep_dest_lab/$reps_yml/$nom_yml"
fi
;;
"INC")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}- include: $champ2 $champ3
EOF
;;
"RUSER")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}  remote_user: $champ2
EOF
;;

"DICVARS"|"LISTVARS")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}${indentvar}${champ2}:
EOF
maxindentdicvar=1
indentdicvar=$(for ((i=0;i<$maxindentdicvar;i++));do printf "   "; done;);
;;
"VARS")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}  vars:
EOF
indentvar=''
indentdicvar=''
maxindentvar=1
indentvar=$(for ((i=0;i<$maxindentvar;i++));do printf "   "; done;);
;;
"VAR")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts
${indent}${indentvar}${indentdicvar}$champ2=$champ3
EOF
;;
"ELMT")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts
${indent}${indentvar}${indentdicvar}- $champ2
EOF
;;
"VARF")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts

EOF
maxindentvar=0
indentvar=''
;;
"VARSFIC")
#http://stackoverflow.com/questions/918886/how-do-i-split-a-string-on-a-delimiter-in-bash
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}vars_files:
EOF
for dest_vars_file in $(echo $champ2 | tr "|" "\n")
do
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}  - "$dest_vars_file"
EOF
done
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml

EOF
;;
"JSONELMT")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts
${indent}${indentvar}${indentdicvar}  - { $champ2 }
EOF
;;
"TABLELMT")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts
${indent}${indentvar}${indentdicvar}  - [ $champ2 ]
EOF
;;
"JSON")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts
${indent}${indentvar}${indentdicvar}  ${champ2}: { $champ3 }
EOF
;;
"TABLE")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts
${indent}${indentvar}${indentdicvar}  ${champ2}: [ $champ2 ]
EOF
;;
"DEPEND")
cat << EOF >> $rep_dest_lab/$reps_hosts/$nom_fic_hosts
dependencies:
EOF
;;

"LAUTH_KEY")
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml

${indent}- name: setup authorized_keys for user $champ2 with local file $champ3 
${indent}  authorized_key: 
${indent}    user: $champ2
${indent}    key: "{{ lookup('file', '$champ3') }}"
EOF
if [ "${champ4-'vide'}" != 'vide'  ]
then
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}    path: $champ4
${indent}    manage_dir: no

EOF
else
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml

EOF
fi
;; # "LAUTH_KEY"
"ROLES")
local liste_roles=`trim $champ2`
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml

${indent}  roles:
EOF
for item_roles in $liste_roles
do
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}    - $item_roles
EOF
done 
;; # "ROLES"
"CREERSUDO")
local creersudo_type=`trim $champ2`
local creersudo_user=`trim $champ3`
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}- name: Generate passwords
${indent}  shell: python -c 'import crypt; print crypt.crypt("$creersudo_user", "!8!rAnd0m_s4lt-!")'
${indent}  register: genpass

${indent}- name: Create ${creersudo_user} group
${indent}  action: group name=${creersudo_user} state=present system=yes

${indent}- name: Create ${creersudo_user} user
${indent}  action: user name=${creersudo_user} password={{ genpass.stdout }} shell=/bin/bash  state=present group=${creersudo_user} comment="${creersudo_user} user" system=yes

${indent}- name: Ensure /etc/sudoers.d directory is present
${indent}  file: path=/etc/sudoers.d state=directory

${indent}- name: Ensure /etc/sudoers.d is scanned by sudo
${indent}  action: lineinfile dest=/etc/sudoers regexp="#includedir\s+/etc/sudoers.d" line="#includedir /etc/sudoers.d"

${indent}- name: Set password to user $creersudo_user
${indent}  shell: echo ${creersudo_user}:${creersudo_user}| sudo chpasswd

EOF

case $creersudo_type in
adm_no_pwd)
cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}- name: Add ${creersudo_user} user to the sudoers
${indent}  action: 'lineinfile dest=/etc/sudoers.d/${creersudo_user} state=present create=yes regexp="${creersudo_user} .*" line="${creersudo_user} ALL=(ALL) NOPASSWD: ALL"'
${indent}  tags: ${creersudo_user}

EOF
;;
esac

cat << EOF >> $rep_dest_lab/$reps_yml/$nom_yml
${indent}- name: Ensure /etc/sudoers.d/${creersudo_user} file has correct permissions
${indent}  action: file path=/etc/sudoers.d/${creersudo_user} mode=0440 state=file owner=root group=root
${indent}  tags: ${creersudo_user}

EOF
;; # "CREERSUDO"
esac # case $champ1 in
champ1=''
champ2=''
champ3=''
champ4=''
champ5=''
champ6=''
done < <(echo "$liste_yml")  # echo $liste_yml|while read ligne
} # gene_yml()

