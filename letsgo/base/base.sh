#!/bin/bash
##################################################################################
# Copyright 2016 Philippe Hayer contact@visualkit.com
# This file is part of vmoopsppaas.
#    This program is free software; you can redistribute it and/or modify it under the terms of the GNU Affero General Public License version 3 as published by the Free Software Foundation with the addition of the following permission added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK IN WHICH THE COPYRIGHT IS OWNED BY vmoopsppaas OWNER, vmoopsppaas OWNER DISCLAIMS THE WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero General Public License along with this program; if not, see http://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA, 02110-1301 USA, or download the license from the following URL: http://www.gnu.org/licenses/agpl.txt
#
#    The interactive user interfaces in modified source and object code versions of this program must display Appropriate Legal Notices, as required under Section 5 of the GNU Affero General Public License.
#
#    In accordance with Section 7(b) of the GNU Affero General Public License, you must retain the producer line in every service that is created or manipulated using vmoopsppaas or display a link to https://gitlab.com/visualkit/vmoopsppaas in any user interface.
#
#    You can be released from the requirements of the license by purchasing a commercial license. Buying such a license is mandatory as soon as you develop commercial activities, direct or not, involving the vmoopsppaas software without disclosing the source code of your own applications. These activities include with no limitation: offering paid services to customers as an ASP, serving Virtual Machines on the fly via  a web application, shipping vmoopsppaas with a closed source product.
###################################################################################

rep_cible='base'

default_user='phil'
default_user_key="/home/${default_user}/.ssh/id_rsa.pub"
master_pass=${default_user}'1234'
vm_maitre_ansible='vm_ansible'
liste_serveurs="test_creer_vm1 $vm_maitre_ansible srv_git_lab"
vbox_version=`virtualbox --help|head -n1|cut -d' ' -f5`

# set -x # passe en mode debug

read -d '' json_serveurs << EOJSON
{
"test_creer_vm1":{
"disksize":2000
},
"$vm_maitre_ansible":{
"disksize":3000
},
"srv_git_lab":{
"disksize":2000
}
}
EOJSON

echo "# definition des vm et groupes de vms"
read -d '' liste_actions <<LISTE
HOST;hosts;./
INITLSTVM;
VM;test_creer_vm1;DYNIP;target_host=test_vm1
VM;srv_git_lab;DYNIP
VM;vm_ansible;DYNIP;target_host=ctrlr_ansible
GRP;docker
 GVM;vm_ansible
GRP;maitre_ansible
 GVM;vm_ansible
GRP;autres
 GVM;test_creer_vm1
 GVM;srv_git_lab
GDG;labo_base_hosts
 GEL;docker
 GEL;maitre_ansible
 GEL;autres
GVARS;labo_base_hosts
 GVAR;mon_groupe;datagroupe

LISTE

echo "#definition des roles"
read -d '' liste_actions2 <<LISTE
INIT
 APT_REP;activer depot backport;deb http://http.debian.net/debian jessie-backports main;state=present
 APT;apt-get update du serveur;update_cache=yes cache_valid_time=3600
 APT;installation de docker;name=docker.io state=present
 APT;installation des outils pythons;name=python-setuptools state=present
 INST_PY;installer pip;pip;state=present
 PIP;virtualenv docker-py
CHREP;RPL;roles/vagrant/tasks
INIT;
 CREERSUDO;adm_no_pwd;vagrant
 LAUTH_KEY;vagrant;$default_user_key
 APT;installation de debconf-utils;name=debconf-utils state=present
 APT;installation de dkms;name=dkms state=present
 ACT;stat;path=/tmp/VBoxGuestAdditions_${vbox_version}.iso
  REGIST;datafic
 SHELL;test presence de VBoxLinuxAdditions;lsmod|grep -i vboxfs|wc -l 
  REGIST;vboxfs
 ACT;get_url;url=http://download.virtualbox.org/virtualbox/${vbox_version}/VBoxGuestAdditions_${vbox_version}.iso dest=/tmp mode=0440
  WHEN;(not datafic.stat.exists) and vboxfs.stdout < 1
 ACT;mount;name=/mnt opts=loop,ro fstype=iso9660 state=mounted src=/tmp/VBoxGuestAdditions_${vbox_version}.iso
  WHEN;datafic.stat.exists
 SHELL;Ajout des additions VBOX linux;echo \$(/mnt/VBoxLinuxAdditions.run);true
  WHEN;(datafic.stat.exists and vboxfs.stdout < 1)
 ACT;command;umount /mnt
  WHEN;datafic.stat.exists
 ACT;file;path=/tmp/VBoxGuestAdditions_${vbox_version}.iso state=absent
 ACT;lineinfile;dest=/etc/fstab regexp=".*VBoxGuestAdditions.*" state=absent
CHREP;RPL;roles/mqtt/tasks
INIT;
 APT;installation de mosquitto mqtt;name=mosquitto state=present
 PIP;paho-mqtt
LISTE

echo "# definition du playbook"
read -d '' liste_actions3 <<LISTE
INIT
HOSTS;labo_base_hosts
 RUSER;$default_user
 SUDO;yes
 ROLES;update
HOSTS;maitre_ansible
 RUSER;$default_user
 SUDO;yes
 ROLES;vagrant mqtt
HOSTS;test_creer_vm1
 RUSER;$default_user
 SUDO;yes
 ROLES;jenkins_host

LISTE

echo "# definition des requis"
read -d '' liste_actions4 <<LISTE
INIT
REQUI;geerlingguy.jenkins
 RQROL;jenkins_host
LISTE



. ../../scripts/creer_infra.sh



