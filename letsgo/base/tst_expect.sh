#!/bin/bash
default_user='phil'
pass1=$default_user
pass2=$pass1'1234'
myPROMPT="\\$"
/usr/bin/expect -d <<EOD
spawn ssh-copy-id -f -o StrictHostKeyChecking=no ${default_user}@localhost
sleep 5
expect {
 "assword:" {
 send  "${pass1}\r"
 send_user "successfully send ssh-copy-id password for user ${default_user}\n"
 exp_continue
 }
 "$myPROMPT\r" {
 send_user "successfully copy ssh id for user ${default_user}\n"
 }
 "try again" {
 send  "${pass2}\r"
 }
 "already exist" {
 send_user "key already exist when copy ssh id for user ${default_user}\n"
}
expect eof
} 
EOD

echo "pour supprimer la clef faire"
echo 'ssh-keygen -f "/srv/phil/.ssh/known_hosts" -R localhost'

